import {TestBed} from '@angular/core/testing';

import {IncrementalIdGeneratorService} from './incremental-id-generator.service';

describe('IncrementalIdGeneratorService', () => {
  let service: IncrementalIdGeneratorService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IncrementalIdGeneratorService]
    });
    service = TestBed.get(IncrementalIdGeneratorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('generateId() method', () => {
    let generatedId: number;
    beforeEach(() => {
      generatedId = service.generateId();
    });

    it('should return 1 at the first time', () => {
      expect(generatedId).toBe(1);
    });
    it('should return 2 at the second time', () => {
      generatedId = service.generateId();
      expect(generatedId).toBe(2);
    });
  });
});
