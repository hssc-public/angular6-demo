import {HttpClient} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {Book} from './book';
import {Injectable} from '@angular/core';
import {catchError, map} from 'rxjs/operators';

@Injectable()
export class BookService {

  constructor(private httpClient: HttpClient) {
  }

  findAll(): Observable<Book[]> {
    return this.httpClient
      .get<Book[]>('http://localhost:8080/books');
  }
  //
  // findAllTitles(): Observable<String[]> {
  //   return this.findAll()
  //     .pipe(map(bookList => bookList.map(book => book.title)));
  // }

  save(bookToSave: Book): Observable<string> {
    return this.httpClient
      .post('http://localhost:8080/books', bookToSave)
      .pipe(map(response => 'OK'))
      .pipe(catchError((err) => {
        console.log('caught error', err);
        return throwError({status: err.status, ...err.error});
      }));
  }
}
