import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.css']
})
export class CounterComponent implements OnInit, OnChanges {

  private _value: number;

  @Input()
  set currentValue(newValue) {
    this._value = newValue;
    console.log('new counter value: ' + newValue);
  }

  get currentValue() {
    return this._value;
  }

  @Output()
  currentValueChanged = new EventEmitter<number>();

  constructor() {
    console.log('CounterComponent.constructor');
  }

  ngOnInit() {
    console.log('CounterComponent.ngOnInit');
  }

  public increment(event: CounterComponent) {
    console.log(event);
    this.currentValue += 1;
    this.currentValueChanged.emit(this.currentValue);
  }

  public decrement() {
    this.currentValue--;
    this.currentValueChanged.emit(this.currentValue);
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log('ngOnChanges', changes);
  }

}
