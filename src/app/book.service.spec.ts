import {inject, TestBed} from '@angular/core/testing';

import {BookService} from './book.service';
import {HttpClientTestingModule, HttpTestingController, TestRequest} from '@angular/common/http/testing';
import {Book} from './book';
import {Observable} from 'rxjs';

fdescribe('BookService', () => {

  let httpTestingController: HttpTestingController;
  let bookService: BookService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [BookService]
    });

    httpTestingController = TestBed.get(HttpTestingController);
    bookService = TestBed.get(BookService);
  });

  it('should be created', inject([BookService], (service: BookService) => {
    expect(service).toBeTruthy();
  }));

  describe('findAll()', () => {
    let nextValueInObservable: Book[] = null;
    it('should send a GET request to /books', () => {
      const result = bookService.findAll();
      result.subscribe(value => nextValueInObservable = value);
      const req = httpTestingController.expectOne('http://localhost:8080/books');
    });
    describe('should return an Observable, which', () => {
      let responseFromServer;
      beforeEach(() => {
        responseFromServer = [
          {isbn: 'isbn1', author: 'A1', title: 'T1'},
          {isbn: 'isbn2', author: 'A2', title: 'T2'}
        ];
        const result = bookService.findAll();
        result.subscribe(value => nextValueInObservable = value);
      });
      it('contains the books, which are replied as a JSON array', () => {
        const req = httpTestingController.expectOne('http://localhost:8080/books');
        req.flush(responseFromServer);
        expect(nextValueInObservable).toEqual(responseFromServer);
      });
    });
  });
  describe('save(bookToSave)', () => {
    let bookToSave;
    let sentRequest: TestRequest;
    let responseObservable: Observable<string>;
    let nextValueInObservable: string;
    let errorInObservable: string;

    beforeEach(() => {
      bookToSave = {isbn: 'ABC', title: 'T1', author: 'A1'};
      responseObservable = bookService.save(bookToSave);
      responseObservable.subscribe(
        next => nextValueInObservable = next,
        error => errorInObservable = error);
      sentRequest = httpTestingController.expectOne('http://localhost:8080/books');
    });

    it('should send a POST request to /books', () => {
      expect(sentRequest).toBeDefined();
    });

    it('should send bookToSave as the body of the request', () => {
      expect(sentRequest.request.body).toEqual(bookToSave);
    });

    it('should send a POST request', () => {
      expect(sentRequest.request.method).toBe('POST');
    });

    describe('when server responds with 422', () => {
      let responseBodyFromServer;
      let respnoseStatusFromServer;
      let respnoseStatusTextFromServer;
      let expectedOnNextValue;
      beforeEach(() => {
        respnoseStatusFromServer = 422;
        respnoseStatusTextFromServer = 'Unprocessable Entity';
        responseBodyFromServer = {code: 'MY CODE', details: 'My details'};
        expectedOnNextValue = {code: 'MY CODE', details: 'My details', status: 422};
        sentRequest.flush(responseBodyFromServer,
          {
            status: respnoseStatusFromServer,
            statusText: respnoseStatusTextFromServer
          });
      });

      describe('The returned observable', () => {
        it('calls subscriber\'s onError with the response body structure with an addition status field with the HTTP status code', () => {
          expect(errorInObservable).toEqual(expectedOnNextValue);
        });
      });
    });

  });

});
