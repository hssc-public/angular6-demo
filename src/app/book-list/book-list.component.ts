import {Component, OnInit} from '@angular/core';
import {BookService} from '../book.service';
import {Book} from '../book';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})
export class BookListComponent implements OnInit {

  bookList: Book[];
  editedBook: Book;
  error: { code: string; details: string, status: number};

  constructor(private bookService: BookService) {
  }

  ngOnInit() {
    this.bookService.findAll()
      .subscribe(bookList => this.bookList = bookList);
  }

  edit(book: Book) {
    this.editedBook = book;
  }

  saveBook(bookToSave: Book) {
    this.error = null;
    this.bookService.save(bookToSave)
      .subscribe(() => console.log('success'),
        error => this.error = error);
  }
}
