import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {RandomGeneratorComponent} from './random-generator.component';
import {RandomGeneratorService} from '../random-generator.service';
import {By} from '@angular/platform-browser';
import {DebugElement} from '@angular/core';

const getGeneratedNumberElement = function (fixture: ComponentFixture<RandomGeneratorComponent>) {
  return fixture.debugElement.query(By.css('#generatedNumber'));
};

const clickOn = function (button: DebugElement) {
  button.triggerEventHandler('click', {});
};
describe('RandomGeneratorComponent', () => {
  let component: RandomGeneratorComponent;
  let fixture: ComponentFixture<RandomGeneratorComponent>;
  let randomGeneratorService: RandomGeneratorService;
  let generateRandomNumber: () => number;
  let generatedNumber;
  let generateButton: DebugElement;

  beforeEach(async(() => {

    generatedNumber = 123;
    generateRandomNumber = jasmine.createSpy('generateRandomNumber')
      .and.returnValue(generatedNumber);

    randomGeneratorService = {
      generateRandomNumber
    };

    TestBed.configureTestingModule({
      declarations: [RandomGeneratorComponent],
      providers: [{provide: RandomGeneratorService, useValue: randomGeneratorService}]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RandomGeneratorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    generateButton = fixture.debugElement.query(By.css('#generateButton'));
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call randomGenerator.generateRandomNumber method when genreateNumber is called', () => {
    component.generateNumber();
    expect(generateRandomNumber).toHaveBeenCalled();
  });

  it('should show no generatedNumber element in DOM', () => {
    const element = getGeneratedNumberElement(fixture);
    expect(element).toBeNull();
  });

  describe('when generatenumber button is clicked', () => {
    beforeEach(() => {
      clickOn(generateButton);
      fixture.detectChanges();
    });

    it('should display the generated number', () => {
      const element = getGeneratedNumberElement(fixture);
      expect(element).toBeDefined();
      expect(element.nativeElement.textContent).toEqual('123');
    });
  });
});
