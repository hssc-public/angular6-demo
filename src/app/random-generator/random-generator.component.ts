import { Component, OnInit } from '@angular/core';
import {RandomGeneratorService} from '../random-generator.service';

@Component({
  selector: 'app-random-generator',
  templateUrl: './random-generator.component.html',
  styleUrls: ['./random-generator.component.css']
})
export class RandomGeneratorComponent implements OnInit {

  lastGeneratedNumber?: number;

  constructor(private randomGenerator: RandomGeneratorService) { }

  generateNumber() {
    this.lastGeneratedNumber = this.randomGenerator.generateRandomNumber();
  }

  ngOnInit() {
  }

}
