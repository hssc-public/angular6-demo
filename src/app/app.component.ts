import {Component} from '@angular/core';
import {Book} from './book';
import {BookService} from './book.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Hello world!';
  book = { title: 'Cim', author: 'Szerzo', isbn: 'ALMAISBN'}
  counterValues: { [key: string]: number } = {
    counter1: 200,
    counter2: 400
  };

  constructor(bookService: BookService) {
    console.log('AppComponent constructor');
  }

  counterValueChanged(counterName, newValue: number) {
    console.log(`Counter ${counterName} has changed: ${newValue}`);
    this.counterValues[counterName] = newValue;

    console.log(this.counterValues);
  }

  noveld() {
    this.counterValues.counter1++;
  }

  harry() {
    this.book = {...this.book, title: 'Harry Potter'};
  }

  bookSaved(book: Book) {
    console.log('root component book saved: ' , book);
  }
}
