import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Book} from '../book';

@Component({
  selector: 'app-book-details-form',
  templateUrl: './book-details-form.component.html',
  styleUrls: ['./book-details-form.component.css']
})
export class BookDetailsFormComponent implements OnInit {

  @Input()
  book: Book;

  @Output()
  bookSaved = new EventEmitter<Book>();

  constructor() { }

  ngOnInit() {
  }

  saved() {
    this.bookSaved.emit(this.book);
  }
}
