import {Component, EventEmitter, Inject, Input, OnInit, Output} from '@angular/core';
import {Book} from '../book';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {BookService} from '../book.service';

@Component({
  selector: 'app-reactive-book-details-form',
  templateUrl: './reactive-book-details-form.component.html',
  styleUrls: ['./reactive-book-details-form.component.css'],
  providers: [BookService]
})
export class ReactiveBookDetailsFormComponent implements OnInit {

  private _book: Book;

  @Input()
  set book(book: Book) {
    this._book = book;
    if (this.bookForm) {
      console.log('set book: ', book);
      this.bookForm.setValue({title: book.title, author: book.author, isbn: book.isbn});
    }
  }

  get book() {
    return this._book;
  }

  @Output()
  bookSaved = new EventEmitter<Book>();
  bookForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private bookService: BookService, bookService2: BookService) {
    console.log('ReactiveBookDetailsFormComponent constructor');
    console.log(bookService === bookService2);
  }

  ngOnInit() {
    this.bookForm = this.formBuilder.group(
      {
        isbn: [this.book.isbn, [Validators.required, Validators.minLength(3)]],
        title: [this.book.title, [Validators.required, Validators.minLength(5)]],
        author: [this.book.author, [Validators.minLength(3)]]
      }
    );

    console.log(this.bookForm);

    this.bookForm.valueChanges.subscribe(newValue => {
      console.log('new Value: ', newValue);

      console.log('bookform errors', this.bookForm.controls.title.errors);
      this._book = newValue;
    });
  }

  saved() {
    if (this.bookForm.valid) {
      this.bookSaved.emit(this.book);
    } else {
      console.log('invalid form');
    }
  }
  }
