import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReactiveBookDetailsFormComponent } from './reactive-book-details-form.component';

describe('ReactiveBookDetailsFormComponent', () => {
  let component: ReactiveBookDetailsFormComponent;
  let fixture: ComponentFixture<ReactiveBookDetailsFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReactiveBookDetailsFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReactiveBookDetailsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
