import {Injectable} from '@angular/core';
import {CounterComponent} from './counter/counter.component';

@Injectable({
  providedIn: 'root'
})
export class RandomGeneratorService {

  constructor() {
  }
    /* ... */

  generateRandomNumber() {
    return Math.ceil(Math.random() * 100);
  }
}
