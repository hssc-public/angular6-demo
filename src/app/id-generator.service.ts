export interface IdGeneratorService {
  generateId(): number;
}

export const IdGenerator = 'IdGenerator';
