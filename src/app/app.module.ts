import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {CounterComponent} from './counter/counter.component';
import {BookDetailsFormComponent} from './book-details-form/book-details-form.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ReactiveBookDetailsFormComponent} from './reactive-book-details-form/reactive-book-details-form.component';
import {BookService} from './book.service';
import {IdGenerator, IdGeneratorService} from './id-generator.service';
import {IncrementalIdGeneratorService} from './incremental-id-generator.service';
import { RandomGeneratorComponent } from './random-generator/random-generator.component';
import {HttpClientModule} from '@angular/common/http';
import { BookListComponent } from './book-list/book-list.component';

@NgModule({
  declarations: [
    AppComponent,
    CounterComponent,
    BookDetailsFormComponent,
    ReactiveBookDetailsFormComponent,
    RandomGeneratorComponent,
    BookListComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    {
      provide: IdGenerator,
      useClass: IncrementalIdGeneratorService
    }, BookService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
