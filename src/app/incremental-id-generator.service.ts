import { Injectable } from '@angular/core';
import {IdGeneratorService} from './id-generator.service';

export class IncrementalIdGeneratorService implements IdGeneratorService {

  counter = 1;

  constructor() {
  }

  generateId(): number {
    return this.counter++;
  }

}
