import {async, TestBed} from '@angular/core/testing';
import {AppComponent} from './app.component';
import {HttpClient, HttpClientModule} from '@angular/common/http';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      imports: [HttpClientModule]
    }).compileComponents();
  }));


  it('hello', (done) => {
    const httpClient: HttpClient = TestBed.get(HttpClient);

    httpClient.get<string>('http://index.hu/')
      .toPromise()
      .then((value => {
        expect(value).toEqual('alma');
        done();
      }), (error) => done.fail('hiba'));
  });

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  it(`should have as title 'app'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app: AppComponent = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('Hello world!');
  }));
  it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Hello world!');
  }));
});
