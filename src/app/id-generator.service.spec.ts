import { TestBed, inject } from '@angular/core/testing';

import {IdGenerator, IdGeneratorService} from './id-generator.service';
import {s} from '@angular/core/src/render3';
import {IncrementalIdGeneratorService} from './incremental-id-generator.service';

describe('IdGeneratorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{provide: IdGenerator, useClass: IncrementalIdGeneratorService}]
    });
  });

  it('should be created', inject([IdGenerator], (service: IdGeneratorService) => {
    expect(service).toBeTruthy();

    const spy = jasmine.createSpy('hello').and.returnValue(345);

    const returnedValue = spy();

    expect(returnedValue).toBe(345);

    expect(spy).toHaveBeenCalled();
  }));
});
